# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_15)


def stmt_pos_76(caller):
    caller.event_declaration('myEvent')


def stmt_pos_103(caller):
    caller.variable_declaration('dumvar', str())


def stmt_pos_163(caller):
    caller.log(lambda x: "1")


def stmt_pos_180(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_201(caller):
    caller.log(lambda x: "2")


def stmt_pos_218(caller):
    caller.assignement('dumvar', lambda x: "ON")


def stmt_pos_242(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_263(caller):
    caller.log(lambda x: "3")


def stmt_pos_280(caller):
    caller.wait_until_expression(lambda x: ureg('3') > ureg('5'), timeout=lambda x: ureg('1s'), raise_event='myEvent')


def stmt_pos_337(caller):
    caller.log(lambda x: "4")


def stmt_pos_354(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_375(caller):
    caller.log(lambda x: "5")


def stmt_pos_508(caller):
    caller.wait_until_expression(lambda x: get_variable(caller, 'dumvar').value == "ON", timeout=None)


def stmt_pos_583(caller):
    caller.log(lambda x: "var wd start")


def stmt_pos_619(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_648(caller):
    caller.log(lambda x: "var wd end")


def stmt_pos_414(caller):
    step = Step_stmt_pos_414(caller)
    caller.watchdogs['Step_stmt_pos_414'] = step
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_414(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.preconditions.append(stmt_pos_508)
        self.main_body.append(stmt_pos_583)
        self.main_body.append(stmt_pos_619)
        self.main_body.append(stmt_pos_648)
        self.confirmation.append(lambda x: ureg('3') > ureg('5'))


def stmt_pos_880(caller):
    caller.wait_for_event(get_event(caller, 'myEvent'), timeout=None)


def stmt_pos_953(caller):
    caller.log(lambda x: "event wd start")


def stmt_pos_991(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_1020(caller):
    caller.log(lambda x: "event wd end")


def stmt_pos_789(caller):
    step = Step_stmt_pos_789(caller)
    caller.watchdogs['Step_stmt_pos_789'] = step
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_789(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.preconditions.append(stmt_pos_880)
        self.main_body.append(stmt_pos_953)
        self.main_body.append(stmt_pos_991)
        self.main_body.append(stmt_pos_1020)
        self.confirmation.append(lambda x: ureg('3') > ureg('5'))


def stmt_pos_15(caller):
    step = Step_stmt_pos_15(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_15(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_76)
        self.declaration.append(stmt_pos_103)
        self.main_body.append(stmt_pos_163)
        self.main_body.append(stmt_pos_180)
        self.main_body.append(stmt_pos_201)
        self.main_body.append(stmt_pos_218)
        self.main_body.append(stmt_pos_242)
        self.main_body.append(stmt_pos_263)
        self.main_body.append(stmt_pos_280)
        self.main_body.append(stmt_pos_337)
        self.main_body.append(stmt_pos_354)
        self.main_body.append(stmt_pos_375)
        self.watchdog_body.append(stmt_pos_414)
        self.watchdog_body.append(stmt_pos_789)

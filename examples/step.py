# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_29)


def stmt_pos_98(caller):
    caller.log(lambda x: "Hello from DummyStep")


def stmt_pos_142(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'DummySatellite/DummyCommand', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_212(caller):
    caller.log(lambda x: "Now calling nested step")


def stmt_pos_348(caller):
    caller.event_declaration('myEvent')


def stmt_pos_387(caller):
    caller.variable_declaration('myVar', int())


def stmt_pos_508(caller):
    caller.log(lambda x: "Hello from NestedStep")


def stmt_pos_561(caller):
    caller.assignement('myVar', lambda x: ureg('123'))


def stmt_pos_599(caller):
    caller.log(lambda x: "myVar:", lambda x: get_variable(caller, 'myVar').value)


def stmt_pos_259(caller):
    step = Step_stmt_pos_259(caller)
    continuation = OrderedDict()
    raise_event = None
    continuation[ConfirmationStatus.CONFIRMED] = ContinuationAction.ASK_USER
    continuation[ConfirmationStatus.NOT_CONFIRMED] = ContinuationAction.ASK_USER
    continuation[ConfirmationStatus.ABORTED] = ContinuationAction.ASK_USER
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_259(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_348)
        self.declaration.append(stmt_pos_387)
        self.main_body.append(stmt_pos_508)
        self.main_body.append(stmt_pos_561)
        self.main_body.append(stmt_pos_599)
        self.confirmation.append(lambda x: get_variable(caller, 'myVar').value < ureg('100'))


def stmt_pos_29(caller):
    step = Step_stmt_pos_29(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_29(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.main_body.append(stmt_pos_98)
        self.main_body.append(stmt_pos_142)
        self.main_body.append(stmt_pos_212)
        self.main_body.append(stmt_pos_259)

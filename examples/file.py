# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_29)
        self.main_body.append(stmt_pos_247)


def stmt_pos_29(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['filepath'] = lambda x: "./dummy.txt"
    activity_call = create_activity_call(caller, 'os/create_file', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    continuation[ConfirmationStatus.NOT_CONFIRMED] = ContinuationAction.ABORT
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_247(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['filepath'] = lambda x: "./dummy.txt"
    activity_call = create_activity_call(caller, 'os/delete_file', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    continuation[ConfirmationStatus.NOT_CONFIRMED] = ContinuationAction.ABORT
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)

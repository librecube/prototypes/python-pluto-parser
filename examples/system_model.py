# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_64)


def stmt_pos_131(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['MONITORING'] = lambda x: ureg('1')
    arguments['STATE'] = lambda x: "ENABLE"
    directives['PROTOCOL_FLAG'] = lambda x: "RESET"
    directives['TX_STATUS'] = lambda x: "ENABLE"
    activity_call = create_activity_call(caller, 'DummySatellite/DummySystem/DummyAction', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_523(caller):
    caller.log(lambda x: get_reporting_data(caller, 'DummySatellite/DummySystem/DummyParameter'))


def stmt_pos_593(caller):
    caller.wait_for_event(get_event(caller, 'DummySatellite/DummySystem/DummyEvent'), timeout=None)


def stmt_pos_64(caller):
    step = Step_stmt_pos_64(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_64(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.main_body.append(stmt_pos_131)
        self.main_body.append(stmt_pos_523)
        self.main_body.append(stmt_pos_593)

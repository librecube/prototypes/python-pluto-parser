# This is auto-generated code from the source Pluto file. Do not modify!


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.declaration.append(stmt_pos_30)
        self.declaration.append(stmt_pos_109)
        self.preconditions.append(stmt_pos_219)
        self.main_body.append(stmt_pos_289)
        self.main_body.append(stmt_pos_324)
        self.main_body.append(stmt_pos_356)
        self.main_body.append(stmt_pos_1205)
        self.main_body.append(stmt_pos_1245)
        self.confirmation.append(stmt_pos_2471)


def stmt_pos_30(caller):
    caller.event_declaration('command_failed', "A remote command did not expected")


def stmt_pos_109(caller):
    caller.event_declaration('bus_switchover', "SpaceCAN bus has switched over")


def stmt_pos_219(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_289(caller):
    caller.log(lambda x: "Starting procedure")


def stmt_pos_324(caller):
    caller.log(lambda x: "Running step 01")


def stmt_pos_426(caller):
    caller.variable_declaration('speed', int())


def stmt_pos_483(caller):
    caller.variable_declaration('delay', int())


def stmt_pos_590(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_669(caller):
    caller.assignement('delay', lambda x: ureg('5s'))


def stmt_pos_700(caller):
    caller.log(lambda x: "Driving forward")


def stmt_pos_740(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['SPEED'] = lambda x: ureg('50')
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/DRIVE_FORWARD', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_905(caller):
    caller.wait_for_relative_time(lambda x: get_variable(caller, 'delay').value)


def stmt_pos_938(caller):
    caller.log(lambda x: "Stopping")


def stmt_pos_971(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/STOP', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1039(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_1116(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_356(caller):
    step = Step_stmt_pos_356(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_356(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_426)
        self.declaration.append(stmt_pos_483)
        self.preconditions.append(stmt_pos_590)
        self.main_body.append(stmt_pos_669)
        self.main_body.append(stmt_pos_700)
        self.main_body.append(stmt_pos_740)
        self.main_body.append(stmt_pos_905)
        self.main_body.append(stmt_pos_938)
        self.main_body.append(stmt_pos_971)
        self.main_body.append(stmt_pos_1039)
        self.confirmation.append(stmt_pos_1116)


def stmt_pos_1205(caller):
    caller.log(lambda x: "Running step 02")


def stmt_pos_1315(caller):
    caller.variable_declaration('speed', int())


def stmt_pos_1372(caller):
    caller.variable_declaration('delay', int())


def stmt_pos_1479(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_1558(caller):
    caller.assignement('delay', lambda x: ureg('5s'))


def stmt_pos_1589(caller):
    caller.log(lambda x: "Driving backward")


def stmt_pos_1630(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['SPEED'] = lambda x: ureg('50')
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/DRIVE_BACKWARD', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1796(caller):
    caller.wait_for_relative_time(lambda x: get_variable(caller, 'delay').value)


def stmt_pos_1829(caller):
    caller.log(lambda x: "Stopping")


def stmt_pos_1862(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    activity_call = create_activity_call(caller, 'ROVER/WHEEL_DRIVE/STOP', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)


def stmt_pos_1930(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_2007(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))


def stmt_pos_1245(caller):
    step = Step_stmt_pos_1245(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_pos_1245(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.declaration.append(stmt_pos_1315)
        self.declaration.append(stmt_pos_1372)
        self.preconditions.append(stmt_pos_1479)
        self.main_body.append(stmt_pos_1558)
        self.main_body.append(stmt_pos_1589)
        self.main_body.append(stmt_pos_1630)
        self.main_body.append(stmt_pos_1796)
        self.main_body.append(stmt_pos_1829)
        self.main_body.append(stmt_pos_1862)
        self.main_body.append(stmt_pos_1930)
        self.confirmation.append(stmt_pos_2007)


def stmt_pos_2471(caller):
    caller.wait_for_relative_time(lambda x: ureg('0.5s'))

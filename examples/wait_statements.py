# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.declaration.append(stmt_pos_97)
        self.preconditions.append(stmt_pos_201)
        self.preconditions.append(stmt_pos_309)
        self.preconditions.append(stmt_pos_326)
        self.preconditions.append(stmt_pos_402)
        self.preconditions.append(lambda x: ureg('10') > ureg('5'))
        self.main_body.append(stmt_pos_568)
        self.confirmation.append(stmt_pos_681)
        self.confirmation.append(stmt_pos_789)
        self.confirmation.append(stmt_pos_865)
        self.confirmation.append(lambda x: ureg('10') > ureg('5'))
        self.confirmation.append(lambda x: ureg('2') < ureg('3'))


def stmt_pos_97(caller):
    caller.event_declaration('myEvent')


def stmt_pos_201(caller):
    caller.wait_until_expression(lambda x: datetime(2001, 8, 18, 21, 7, 43, 137468), timeout=None)


def stmt_pos_309(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_326(caller):
    caller.wait_for_relative_time(lambda x: ureg('2s'))


def stmt_pos_402(caller):
    caller.wait_until_expression(lambda x: ureg('10') > ureg('5'), timeout=None)


def stmt_pos_568(caller):
    caller.log(lambda x: "hello", lambda x: "world", lambda x: "!")


def stmt_pos_681(caller):
    caller.wait_until_expression(lambda x: datetime(2001, 8, 18, 21, 7, 43, 137468), timeout=None)


def stmt_pos_789(caller):
    caller.wait_for_relative_time(lambda x: ureg('1s'))


def stmt_pos_865(caller):
    caller.wait_until_expression(lambda x: ureg('10') > ureg('5'), timeout=None)

# This is auto-generated code from the source Pluto file.
from pluto_executor.language import *


class Procedure_(Procedure):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_pos_61)
        self.main_body.append(stmt_pos_545)


def stmt_pos_61(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['MONITORING'] = lambda x: ureg('1')
    arguments['STATE'] = lambda x: "ENABLE"
    arguments['PATCH'] = dict()
    arguments['PATCH']['byte1'] = lambda x: ureg('1')
    arguments['PATCH']['byte2'] = lambda x: ureg('2')
    directives['PROTOCOL_FLAG'] = lambda x: "RESET"
    directives['TX_STATUS'] = lambda x: "ENABLE"
    activity_call = create_activity_call(caller, 'DummySatellite/DummyCommand', arguments, directives)
    caller.refer_by['ref1'] = caller.initiate_activity(activity_call)


def stmt_pos_545(caller):
    arguments = OrderedDict()
    directives = OrderedDict()
    arguments['MONITORING'] = lambda x: ureg('2')
    arguments['STATE'] = lambda x: "DISABLE"
    directives['PROTOCOL_FLAG'] = lambda x: "ENABLE"
    directives['TX_STATUS'] = lambda x: "ENABLE"
    activity_call = create_activity_call(caller, 'DummySatellite/DummyCommand', arguments, directives)
    continuation = OrderedDict()
    raise_event = None
    continuation[ConfirmationStatus.CONFIRMED] = ContinuationAction.CONTINUE
    continuation[ConfirmationStatus.NOT_CONFIRMED] = ContinuationAction.ABORT
    continuation[ConfirmationStatus.ABORTED] = ContinuationAction.ABORT
    caller.refer_by['ref2'] = caller.initiate_and_confirm_activity(activity_call, continuation, raise_event)

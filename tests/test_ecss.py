from pathlib import Path

import pytest

from pluto_parser import parse, pluto_parse


EXAMPLES_FOLDER = "tests/ecss7032"
EXAMPLE_DIR = Path(__file__).parent.parent / EXAMPLES_FOLDER
EXAMPLES = sorted(
    [e.relative_to(EXAMPLE_DIR) for e in EXAMPLE_DIR.glob("*.pluto")]
)


@pytest.mark.parametrize("example", EXAMPLES, ids=str)
def test_examples_parse(example):
    """The available pluto examples parse successfully.

    Note that this does not mean that the parse or source is correct."""
    with open(EXAMPLE_DIR / (str(example))) as f:
        contents = f.read()
    tree = parse(contents)
    script = pluto_parse(contents)


from functools import partial, lru_cache

import pytest

from pluto_parser import parser


ENG_UNIT_EXAMPLES = (
    # Unit examples from ECSS-E-ST-70-32C(31July2008) section B.2, and others
    "m",
    "AU",
    "pc",
    "L",
    "g",
    "u",
    "t",
    "s",
    "min",
    "h",
    "d",
    "A",
    "K",
    "degC",
    "mol",
    "cd",
    "rad",
    "r",
    "deg",
    "arcmin",
    "arcsec",
    "sr",
    "Hz",
    "N",
    "Pa",
    "bar",
    "J",
    "eV",
    "W",
    "C",
    "V",
    "F",
    "Ohm",
    "S",
    "Wb",
    "T",
    "H",
    "lm",
    "lx",
    "dB",
    "Np",
    "Bq",
    "Gy",
    "Sv",
    "bit",
    "B",
    "Bd",
    "m^2",
    "m^3",
    "s^-1",
    "m/s",
    "rad/s",
    "deg/s",
    "m/s^2",
    "m^-1",
    "kg/m^3",
    "kg/m",
    "kg.m/s",
    "kg.m^2/s",
    "kg.m^2",
    "Pa.s",
    "N.m",
    "Pa.s/m",
    "Pa.s/m^3",
    "m^2/s",
    "m^3/s",
    "N/m",
    "K^-1",
    "W/(m.K)",
    "W/(m^2.K)",
    "J/K",
    "J/(kg.K)",
    "J/kg",
    "C/m^3",
    "C/m^2",
    "V/m",
    "F/m",
    "C.m",
    "A/m^2",
    "A/m",
    "A.h",
    "Wb/m",
    "H/m",
    "A.m^2",
    "A/m",
    "Wb.m",
    "Ohm.m",
    "S/m",
    "H^-1",
    "W/sr",
    "W/(m^2.sr)",
    "W/m^2",
    "lm.s",
    "cd/m^2",
    "lm/m^2",
    "lx.s",
    "lm/W",
    "N.s/m",
    "kg/mol",
    "m^3/mol",
    "J/mol",
    "J/(mol.K)",
    "mol/m^3",
    "bit/s",
    #  Additional examples from here
    "kV",
    "mm",
    "deg/h",
)


@pytest.fixture(scope="session", params=["earley", "lalr"])
def eng_parser(request):
    # Make sure debug is enabled by default
    eng_parser = parser.partial_eng_units_parser(
        debug=True,
        parser=request.param,
    )
    return eng_parser


@pytest.fixture(scope="module")
def pluto_parser():
    # Make sure debug is enabled by default
    debug_parser = partial(
        parser.partial_parser,
        debug=True,
    )
    # memoize to save time on repeated use
    pluto_parser = lru_cache()(debug_parser)
    return pluto_parser


@pytest.fixture(params=ENG_UNIT_EXAMPLES)
def unit_example(request):
    return request.param
